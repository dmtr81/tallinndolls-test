<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201211064007 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Used value object for amount.';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE invoices CHANGE amount amount VARCHAR(255) NOT NULL COMMENT \'(DC2Type:money)\'');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE invoices CHANGE amount amount VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}

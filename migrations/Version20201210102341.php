<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201210102341 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Table to store invoices.';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('
            CREATE TABLE invoices (
                id VARCHAR(255) NOT NULL,
                amount VARCHAR(255) NOT NULL,
                uploaded_on DATE NOT NULL COMMENT \'(DC2Type:date_immutable)\',
                due_on DATE NOT NULL COMMENT \'(DC2Type:date_immutable)\',
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE invoices');
    }
}

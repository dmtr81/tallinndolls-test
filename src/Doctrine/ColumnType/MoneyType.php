<?php

namespace App\Doctrine\ColumnType;

use Money\Money;
use Money\Currency;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;

final class MoneyType extends Type
{
    public const TYPE_NAME = 'money';
    private const AMOUNT_CURRENCY_DELIMITER = ' ';

    /**
     * @param mixed[] $fieldDeclaration
     */
    public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return $platform->getVarcharTypeDeclarationSQL($fieldDeclaration);
    }

    /**
     * @inheritDoc
     *
     * @phpcsSuppress SlevomatCodingStandard.Functions.UnusedParameter
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }

    /**
     * @inheritDoc
     *
     * @phpcsSuppress SlevomatCodingStandard.Functions.UnusedParameter
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?Money
    {
        if ($value === null) {
            return null;
        }

        [$currency, $amount] = explode(self::AMOUNT_CURRENCY_DELIMITER, (string) $value, 2);

        return new Money((int) $amount, new Currency($currency));
    }

    /**
     * @inheritDoc
     *
     * @phpcsSuppress SlevomatCodingStandard.Functions.UnusedParameter
     */
    public function convertToDatabaseValue($money, AbstractPlatform $platform)
    {
        if ($money === null) {
            return null;
        }

        if ($money instanceof Money) {
            return implode(self::AMOUNT_CURRENCY_DELIMITER, [(string) $money->getCurrency(), $money->getAmount()]);
        }

        throw ConversionException::conversionFailed(
            is_object($money) ? get_class($money) : (string) $money,
            self::TYPE_NAME,
        );
    }

    public function getName(): string
    {
        return self::TYPE_NAME;
    }
}

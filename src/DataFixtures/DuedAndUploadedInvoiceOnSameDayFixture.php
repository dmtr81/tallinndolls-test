<?php

namespace App\DataFixtures;

use App\Invoice\Entity\Invoice;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Money\Currency;
use Money\Money;

class DuedAndUploadedInvoiceOnSameDayFixture extends Fixture
{
    public const REFERENCE_NAME = 'dued-and-uploaded-invoice-on-same-day';

    public function load(ObjectManager $manager)
    {
        $today = new \DateTimeImmutable();

        $invoice = new Invoice(
            uniqid(),
            new Money('10000', new Currency('USD')),
            $today,
            $today
        );

        $manager->persist($invoice);
        $manager->flush();

        $this->addReference(self::REFERENCE_NAME, $invoice);
    }
}

<?php

namespace App\Twig;

use Money\Money;
use Money\MoneyFormatter;

final class MoneyFilter
{
    private MoneyFormatter $moneyFormatter;

    public function __construct(MoneyFormatter $moneyFormatter)
    {
        $this->moneyFormatter = $moneyFormatter;
    }

    public function __invoke(Money $money): string
    {
        return sprintf(
            '%s %s',
            $this->moneyFormatter->format($money),
            $money->getCurrency()->getCode(),
        );
    }
}

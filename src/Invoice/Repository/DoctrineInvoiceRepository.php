<?php

namespace App\Invoice\Repository;

use App\Invoice\Entity\Invoice;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

final class DoctrineInvoiceRepository implements InvoiceRepositoryInterface
{
    private EntityManagerInterface $entityManager;
    private EntityRepository $doctrineInvoiceRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->doctrineInvoiceRepository = $entityManager->getRepository(Invoice::class);
    }

    public function findById(string $id): ?Invoice
    {
        return $this->doctrineInvoiceRepository->find($id);
    }

    /**
     * @return Invoice[]
     */
    public function findAll(): array
    {
        return $this->doctrineInvoiceRepository->findAll();
    }

    public function persist(Invoice $invoice): void
    {
        $this->entityManager->persist($invoice);
    }

    public function flush(): void
    {
        $this->entityManager->flush();
    }
}

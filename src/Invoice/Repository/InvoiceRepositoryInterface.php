<?php

namespace App\Invoice\Repository;

use App\Invoice\Entity\Invoice;

interface InvoiceRepositoryInterface
{
    public function findById(string $id): ?Invoice;

    /**
     * @return Invoice[]
     */
    public function findAll(): array;

    public function persist(Invoice $invoice): void;

    public function flush(): void;
}

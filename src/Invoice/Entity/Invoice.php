<?php

namespace App\Invoice\Entity;

use Doctrine\ORM\Mapping as ORM;
use Money\Money;

/**
 * @ORM\Entity()
 * @ORM\Table(name="invoices")
 *
 * @final
 */
class Invoice
{
    private const EARLY_INVOICE_UPLOAD_PERIOD_IN_DAYS = 30;
    private const EARLY_INVOICE_UPLOAD_SELL_PRICE_COEFFICIENT = '0.5';
    private const LATE_INVOICE_UPLOAD_SELL_PRICE_COEFFICIENT = '0.3';

    /**
     * @ORM\Id
     * @ORM\Column(type="string", unique=true)
     */
    private string $id;

    /**
     * @ORM\Column(type="money")
     */
    private Money $amount;

    /**
     * @ORM\Column(type="date_immutable")
     */
    private \DateTimeImmutable $uploadedOn;

    /**
     * @ORM\Column(type="date_immutable")
     */
    private \DateTimeImmutable $dueOn;

    public function __construct(string $id, Money $amount, \DateTimeImmutable $uploadedOn, \DateTimeImmutable $dueOn)
    {
        self::assertAmountMustNotBeNegative($amount);

        $this->id = $id;
        $this->amount = $amount;
        $this->uploadedOn = $uploadedOn;
        $this->dueOn = $dueOn;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getAmount(): Money
    {
        return $this->amount;
    }

    public function getUploadedOn(): \DateTimeImmutable
    {
        return $this->uploadedOn;
    }

    public function getDueOn(): \DateTimeImmutable
    {
        return $this->dueOn;
    }

    public function getSellPrice(): Money
    {
        if ($this->getDaysBetweenUploadedDateAndDueDate() > self::EARLY_INVOICE_UPLOAD_PERIOD_IN_DAYS) {
            return $this->getAmount()->multiply(self::EARLY_INVOICE_UPLOAD_SELL_PRICE_COEFFICIENT);
        }

        return $this->getAmount()->multiply(self::LATE_INVOICE_UPLOAD_SELL_PRICE_COEFFICIENT);
    }

    private function getDaysBetweenUploadedDateAndDueDate(): int
    {
        if ($this->getUploadedOn() >= $this->getDueOn()) {
            return 0;
        }

        return $this->getUploadedOn()->diff($this->getDueOn())->days ?: 0;
    }

    private static function assertAmountMustNotBeNegative(Money $amount): void
    {
        if ($amount->isNegative()) {
            throw new \LogicException('Amount must not be negative.');
        }
    }
}

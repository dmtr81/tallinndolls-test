<?php

namespace App\Invoice\Import\Report;

final class InvoicesImportReport
{
    private int $numberOfSuccessfulImportedInvoices = 0;

    /** @var string[] */
    private array $failureMessages;

    public function __construct(int $numberOfSuccessfulImportedInvoices, array $failureMessages)
    {
        $this->numberOfSuccessfulImportedInvoices = $numberOfSuccessfulImportedInvoices;
        $this->failureMessages = $failureMessages;
    }

    public function getNumberOfSuccessfulImportedInvoices(): int
    {
        return $this->numberOfSuccessfulImportedInvoices;
    }

    /**
     * @return string[]
     */
    public function getFailureMessages(): array
    {
        return $this->failureMessages;
    }
}

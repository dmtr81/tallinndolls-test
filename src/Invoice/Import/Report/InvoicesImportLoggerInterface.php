<?php

namespace App\Invoice\Import\Report;

interface InvoicesImportLoggerInterface
{
    public function logSuccessfulRowImport(\SplFileInfo $invoicesSheetFileInfo): void;

    public function logFailedRowImport(\SplFileInfo $invoicesSheetFileInfo, string $failureMessage): void;

    public function getFreshReport(\SplFileInfo $invoicesSheetFileInfo): InvoicesImportReport;
}

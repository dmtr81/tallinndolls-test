<?php

namespace App\Invoice\Import\Report;

final class InMemoryInvoicesImportLogger implements InvoicesImportLoggerInterface
{
    private array $numberOfSuccessfulImportedInvoicesFromFile = [];
    private array $failureMessagesFromFile = [];

    public function logSuccessfulRowImport(\SplFileInfo $invoicesSheetFileInfo): void
    {
        $sheetId = self::getSheetFileId($invoicesSheetFileInfo);

        $this->numberOfSuccessfulImportedInvoicesFromFile[$sheetId] ??= 0;
        $this->numberOfSuccessfulImportedInvoicesFromFile[$sheetId]++;
    }

    public function logFailedRowImport(\SplFileInfo $invoicesSheetFileInfo, string $failureMessage): void
    {
        $sheetId = self::getSheetFileId($invoicesSheetFileInfo);

        $this->failureMessagesFromFile[$sheetId] ??= [];
        $this->failureMessagesFromFile[$sheetId][] = $failureMessage;
    }

    public function getFreshReport(\SplFileInfo $invoicesSheetFileInfo): InvoicesImportReport
    {
        $sheetId = self::getSheetFileId($invoicesSheetFileInfo);

        return new InvoicesImportReport(
            $this->numberOfSuccessfulImportedInvoicesFromFile[$sheetId] ?? 0,
            $this->failureMessagesFromFile[$sheetId] ?? []
        );
    }

    private static function getSheetFileId(\SplFileInfo $invoicesSheetFileInfo): string
    {
        return spl_object_hash($invoicesSheetFileInfo);
    }
}

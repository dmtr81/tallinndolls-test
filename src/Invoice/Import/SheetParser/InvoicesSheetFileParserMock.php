<?php

namespace App\Invoice\Import\SheetParser;

class InvoicesSheetFileParserMock implements InvoicesSheetFileParserInterface
{
    /** @var InvoiceRow[] */
    private array $expectedInvoicesRows;

    public function __construct(InvoiceRow ...$expectedInvoicesRows)
    {
        $this->expectedInvoicesRows = $expectedInvoicesRows;
    }

    public function parseFile(\SplFileInfo $invoicesSheetFileInfo): \Generator
    {
        yield from $this->expectedInvoicesRows;
    }
}

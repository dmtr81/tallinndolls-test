<?php

namespace App\Invoice\Import\SheetParser;

interface InvoicesSheetFileParserInterface
{
    /**
     * @param \SplFileInfo $invoicesSheetFileInfo
     *
     * @return InvoiceRow[]|\Generator
     */
    public function parseFile(\SplFileInfo $invoicesSheetFileInfo): \Generator;
}

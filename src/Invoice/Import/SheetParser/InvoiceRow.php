<?php

namespace App\Invoice\Import\SheetParser;

use App\Validator\Constraint\EntityNotExist\InvoiceIsNotExist;
use Symfony\Component\Validator\Constraints as Assert;

final class InvoiceRow
{
    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(max="255")
     *
     * @InvoiceIsNotExist()
     */
    public $invoiceId;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @Assert\Regex(pattern="/\d+(\.\d+)?/", message="Value must be a valid amount.")
     * @Assert\PositiveOrZero()
     */
    public $invoiceAmount;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Date()
     */
    public $invoiceDueOn;
}

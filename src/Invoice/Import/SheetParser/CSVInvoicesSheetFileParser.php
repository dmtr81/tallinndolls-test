<?php

namespace App\Invoice\Import\SheetParser;

use App\Module\CSVSheetParser\CsvFileParserInterface;
use App\Module\CSVSheetParser\Sheet\Row;

final class CSVInvoicesSheetFileParser implements InvoicesSheetFileParserInterface
{
    private const ID_FIELD_NUMBER = 0;
    private const AMOUNT_FIELD_NUMBER = 1;
    private const DUE_ON_FIELD_NUMBER = 2;

    private CsvFileParserInterface $csvFileParser;

    public function __construct(CsvFileParserInterface $csvFileParser)
    {
        $this->csvFileParser = $csvFileParser;
    }

    /**
     * @param \SplFileInfo $invoicesSheetFileInfo
     *
     * @return InvoiceRow[]|\Generator
     */
    public function parseFile(\SplFileInfo $invoicesSheetFileInfo): \Generator
    {
        foreach ($this->csvFileParser->parseFile($invoicesSheetFileInfo) as $rowNumber => $row) {
            yield $this->createInvoiceRowFromSheet($row);
        }
    }

    private function createInvoiceRowFromSheet(Row $row): InvoiceRow
    {
        $invoiceRow = new InvoiceRow();
        $invoiceRow->invoiceId = self::normalizeIdentifier((string) $row->findFieldByNumber(self::ID_FIELD_NUMBER));
        $invoiceRow->invoiceAmount = (string) $row->findFieldByNumber(self::AMOUNT_FIELD_NUMBER);
        $invoiceRow->invoiceDueOn = (string) $row->findFieldByNumber(self::DUE_ON_FIELD_NUMBER);

        return $invoiceRow;
    }

    private static function normalizeIdentifier(string $id): string
    {
        return strtolower($id);
    }
}

<?php

namespace App\Invoice\Import\RowValidator\Exception;

final class InvoiceRowIsInvalidException extends \RuntimeException
{
}

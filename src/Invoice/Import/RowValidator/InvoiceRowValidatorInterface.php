<?php

namespace App\Invoice\Import\RowValidator;

use App\Invoice\Import\RowValidator\Exception\InvoiceRowIsInvalidException;
use App\Invoice\Import\SheetParser\InvoiceRow;

interface InvoiceRowValidatorInterface
{
    /**
     * @throws InvoiceRowIsInvalidException
     */
    public function assertRowValidity(InvoiceRow $invoiceRow): void;
}

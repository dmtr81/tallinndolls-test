<?php

namespace App\Invoice\Import\RowValidator;

use App\Invoice\Import\RowValidator\Exception\InvoiceRowIsInvalidException;
use App\Invoice\Import\SheetParser\InvoiceRow;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SymfonyInvoiceRowValidator implements InvoiceRowValidatorInterface
{
    private ValidatorInterface $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function assertRowValidity(InvoiceRow $invoiceRow): void
    {
        $violations = $this->validator->validate($invoiceRow);

        if (!count($violations)) {
            return;
        }

        throw new InvoiceRowIsInvalidException(sprintf('Row values are invalid. %s', (string) $violations));
    }
}

<?php

namespace App\Invoice\Import;

use App\Invoice\Entity\Invoice;
use App\Invoice\Import\Report\ImportInvoiceFailedEvent;
use App\Invoice\Import\Report\InvoiceSuccessfulImportedEvent;
use App\Invoice\Import\RowValidator\InvoiceRowValidatorInterface;
use App\Invoice\Import\SheetParser\InvoiceRow;
use App\Invoice\Import\SheetParser\InvoicesSheetFileParserInterface;
use App\Invoice\Repository\InvoiceRepositoryInterface;
use Money\Currency;
use Money\MoneyParser;
use Psr\EventDispatcher\EventDispatcherInterface;

final class InvoicesSheetImporter
{
    private InvoiceRepositoryInterface $invoiceRepository;
    private InvoiceRowValidatorInterface $invoiceRowValidator;
    private EventDispatcherInterface $eventDispatcher;
    private MoneyParser $moneyParser;

    public function __construct(
        InvoiceRepositoryInterface $invoiceRepository,
        InvoiceRowValidatorInterface $invoiceRowValidator,
        EventDispatcherInterface $eventDispatcher,
        MoneyParser $moneyParser
    ) {
        $this->invoiceRepository = $invoiceRepository;
        $this->invoiceRowValidator = $invoiceRowValidator;
        $this->eventDispatcher = $eventDispatcher;
        $this->moneyParser = $moneyParser;
    }

    public function importSheet(\SplFileInfo $invoicesSheetFileInfo, InvoicesSheetFileParserInterface $sheetFileParser): void
    {
        $uploadedOn = new \DateTimeImmutable();

        foreach ($sheetFileParser->parseFile($invoicesSheetFileInfo) as $rowNumber => $invoiceRow) {
            try {
                $invoice = $this->createValidInvoiceByRow($invoiceRow, $uploadedOn);

                $this->invoiceRepository->persist($invoice);

                $this->eventDispatcher->dispatch(new InvoiceSuccessfulImportedEvent($invoicesSheetFileInfo, $invoiceRow, $invoice));
            } catch (\Throwable $exception) {
                $this->eventDispatcher->dispatch(new ImportInvoiceFailedEvent($invoicesSheetFileInfo, $invoiceRow, $rowNumber, $exception));
            }
        }

        $this->invoiceRepository->flush();
    }

    private function createValidInvoiceByRow(InvoiceRow $invoiceRow, \DateTimeImmutable $uploadedOn): Invoice
    {
        $this->invoiceRowValidator->assertRowValidity($invoiceRow);

        $amount = $this->moneyParser->parse($invoiceRow->invoiceAmount, new Currency('USD')); // @todo hardcoded currency
        $invoiceDueOn = new \DateTimeImmutable($invoiceRow->invoiceDueOn);

        return new Invoice($invoiceRow->invoiceId, $amount, $uploadedOn, $invoiceDueOn);
    }
}

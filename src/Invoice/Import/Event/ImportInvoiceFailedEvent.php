<?php

namespace App\Invoice\Import\Report;

use App\Invoice\Import\SheetParser\InvoiceRow;

final class ImportInvoiceFailedEvent
{
    private \SplFileInfo $invoicesSheetFileInfo;
    private InvoiceRow $invoiceRow;
    private int $rowNumber;
    private \Throwable $exception;

    public function __construct(\SplFileInfo $invoicesSheetFileInfo, InvoiceRow $invoiceRow, int $rowNumber, \Throwable $exception)
    {
        $this->invoicesSheetFileInfo = $invoicesSheetFileInfo;
        $this->invoiceRow = $invoiceRow;
        $this->rowNumber = $rowNumber;
        $this->exception = $exception;
    }

    public function getInvoicesSheetFileInfo(): \SplFileInfo
    {
        return $this->invoicesSheetFileInfo;
    }

    public function getInvoiceRow(): InvoiceRow
    {
        return $this->invoiceRow;
    }

    public function getRowNumber(): int
    {
        return $this->rowNumber;
    }

    public function getException(): \Throwable
    {
        return $this->exception;
    }
}

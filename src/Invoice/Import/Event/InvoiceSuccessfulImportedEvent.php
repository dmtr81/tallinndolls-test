<?php

namespace App\Invoice\Import\Report;

use App\Invoice\Entity\Invoice;
use App\Invoice\Import\SheetParser\InvoiceRow;

final class InvoiceSuccessfulImportedEvent
{
    private \SplFileInfo $invoicesSheetFileInfo;
    private InvoiceRow $invoiceRow;
    private Invoice $invoice;

    public function __construct(\SplFileInfo $invoicesSheetFileInfo, InvoiceRow $invoiceRow, Invoice $invoice)
    {
        $this->invoicesSheetFileInfo = $invoicesSheetFileInfo;
        $this->invoiceRow = $invoiceRow;
        $this->invoice = $invoice;
    }

    public function getInvoicesSheetFileInfo(): \SplFileInfo
    {
        return $this->invoicesSheetFileInfo;
    }

    public function getInvoiceRow(): InvoiceRow
    {
        return $this->invoiceRow;
    }

    public function getInvoice(): Invoice
    {
        return $this->invoice;
    }
}

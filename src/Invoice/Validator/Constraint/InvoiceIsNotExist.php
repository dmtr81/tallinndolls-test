<?php

namespace App\Validator\Constraint\EntityNotExist;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
final class InvoiceIsNotExist extends Constraint
{
    public string $message = 'The invoice already exists';
}

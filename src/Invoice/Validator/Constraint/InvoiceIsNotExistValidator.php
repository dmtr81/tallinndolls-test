<?php

namespace App\Validator\Constraint\EntityNotExist;

use App\Invoice\Repository\InvoiceRepositoryInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

final class InvoiceIsNotExistValidator extends ConstraintValidator
{
    private InvoiceRepositoryInterface $invoiceRepository;

    public function __construct(InvoiceRepositoryInterface $invoiceRepository)
    {
        $this->invoiceRepository = $invoiceRepository;
    }

    /**
     * @inheritDoc
     */
    public function validate($invoiceId, Constraint $constraint): void
    {
        self::assertConstraintIsInvoiceIsNotExist($constraint);

        if (!$invoiceId || !is_string($invoiceId)) {
            return;
        }

        $invoice = $this->invoiceRepository->findById($invoiceId);

        if ($invoice) {
            $this->context->addViolation($constraint->message);
        }
    }

    private static function assertConstraintIsInvoiceIsNotExist(Constraint $constraint): void
    {
        if (!$constraint instanceof InvoiceIsNotExist) {
            throw new \InvalidArgumentException(sprintf('Constraint must be instance %s', InvoiceIsNotExist::class));
        }
    }
}

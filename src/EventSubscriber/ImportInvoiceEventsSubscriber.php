<?php

namespace App\EventSubscriber;

use App\Invoice\Import\Report\ImportInvoiceFailedEvent;
use App\Invoice\Import\Report\InvoicesImportLoggerInterface;
use App\Invoice\Import\Report\InvoiceSuccessfulImportedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

final class ImportInvoiceEventsSubscriber implements EventSubscriberInterface
{
    private InvoicesImportLoggerInterface $invoicesImportLogger;

    public function __construct(InvoicesImportLoggerInterface $invoicesImportLogger)
    {
        $this->invoicesImportLogger = $invoicesImportLogger;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            InvoiceSuccessfulImportedEvent::class => 'logSuccessfulInvoiceImport',
            ImportInvoiceFailedEvent::class => 'logInvoiceImportFailure',
        ];
    }

    public function logSuccessfulInvoiceImport(InvoiceSuccessfulImportedEvent $event): void
    {
        $this->invoicesImportLogger->logSuccessfulRowImport($event->getInvoicesSheetFileInfo());
    }

    public function logInvoiceImportFailure(ImportInvoiceFailedEvent $event): void
    {
        $message = sprintf(
            'Failed to import invoice row %d (%s). %s',
            $event->getRowNumber(),
            json_encode($event->getInvoiceRow()),
            $event->getException()->getMessage()
        );

        $this->invoicesImportLogger->logFailedRowImport($event->getInvoicesSheetFileInfo(), $message);
    }
}

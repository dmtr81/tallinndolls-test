<?php

namespace App\Controller;

use App\Invoice\Form\UploadInvoicesSheetFormType;
use App\Invoice\Import\InvoicesSheetImporter;
use App\Invoice\Import\Report\InvoicesImportLoggerInterface;
use App\Invoice\Import\SheetParser\CSVInvoicesSheetFileParser;
use App\Invoice\Repository\InvoiceRepositoryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class InvoiceController extends AbstractController
{
    private InvoicesSheetImporter $invoicesImporter;
    private InvoiceRepositoryInterface $invoiceRepository;
    private CSVInvoicesSheetFileParser $csvInvoicesSheetFileParser;
    private InvoicesImportLoggerInterface $invoicesImportLogger;

    public function __construct(
        InvoicesSheetImporter $invoicesImporter,
        InvoiceRepositoryInterface $invoiceRepository,
        CSVInvoicesSheetFileParser $csvInvoicesSheetFileParser,
        InvoicesImportLoggerInterface $invoicesImportLogger
    ){
        $this->invoicesImporter = $invoicesImporter;
        $this->invoiceRepository = $invoiceRepository;
        $this->csvInvoicesSheetFileParser = $csvInvoicesSheetFileParser;
        $this->invoicesImportLogger = $invoicesImportLogger;
    }

    /**
     * @Route("/", name="invoices")
     */
    public function list(): Response
    {
        $uploadInvoicesSheetForm = $this->createForm(UploadInvoicesSheetFormType::class, [], [
            'action' => $this->generateUrl('invoices_import'),
        ]);

        return $this->render('invoice/list.html.twig', [
            'uploadInvoicesSheetForm' => $uploadInvoicesSheetForm->createView(),
            'invoices' => $this->invoiceRepository->findAll(), // @todo pagination required
        ]);
    }

    /**
     * @Route("/import", name="invoices_import")
     */
    public function importSheet(Request $request): Response
    {
        $importReport = null;

        $uploadInvoicesSheetForm = $this->createForm(UploadInvoicesSheetFormType::class);
        $uploadInvoicesSheetForm->handleRequest($request);

        if ($uploadInvoicesSheetForm->isSubmitted() && $uploadInvoicesSheetForm->isValid()) {
            $invoicesSheetFile = $uploadInvoicesSheetForm->getData()['invoicesSheetFile'];

            $this->invoicesImporter->importSheet($invoicesSheetFile, $this->csvInvoicesSheetFileParser);

            $importReport = $this->invoicesImportLogger->getFreshReport($invoicesSheetFile);
        }

        return $this->render('invoice/import_report.html.twig', [
            'uploadInvoicesSheetForm' => $uploadInvoicesSheetForm->createView(),
            'importReport' => $importReport,
        ]);
    }
}

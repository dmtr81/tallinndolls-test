<?php

namespace App\Module\CSVSheetParser\Exception;

final class FileIsNotExistException extends \RuntimeException
{
}

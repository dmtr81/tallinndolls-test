<?php

namespace App\Module\CSVSheetParser;

use App\Module\CSVSheetParser\Sheet\Field;
use App\Module\CSVSheetParser\Sheet\Row;

final class CsvFileParser implements CsvFileParserInterface
{
    public function parseFile(\SplFileInfo $sourceFileInfo): \Generator
    {
        self::assertSourceFileIsExists($sourceFileInfo);

        $sourceFileObject = $sourceFileInfo->openFile('r');
        $sourceFileObject->setFlags(\SplFileObject::SKIP_EMPTY);

        while (!$sourceFileObject->eof() && $row = $sourceFileObject->fgetcsv()) {
            $fields = [];

            foreach ($row as $fieldNumber => $value) {
                $fields[] = new Field($fieldNumber, trim($value));
            }

            yield new Row($fields);
        }
    }

    private static function assertSourceFileIsExists(\SplFileInfo $sourceFile): void
    {
        if (!$sourceFile->isFile()) {
            throw new \InvalidArgumentException(sprintf(
                'Source file "%s" is not exists or is not a file.',
                $sourceFile->getRealPath(),
            ));
        }
    }
}

<?php

namespace App\Module\CSVSheetParser\Sheet;

final class Field
{
    private int $number;
    private string $value;

    public function __construct(int $number, string $value)
    {
        $this->number = $number;
        $this->value = $value;
    }

    public function getNumber(): int
    {
        return $this->number;
    }

    public function __toString(): string
    {
        return $this->value;
    }
}

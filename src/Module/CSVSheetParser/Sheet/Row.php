<?php

namespace App\Module\CSVSheetParser\Sheet;

final class Row
{
    /** @var Field[] */
    private array $fields = [];

    /**
     * @param Field[] $items
     */
    public function __construct(array $items)
    {
        self::assertItemsAreFields($items);

        $this->fields = $items;
    }

    public function findFieldByNumber(int $number): ?Field
    {
        foreach ($this->fields as $field) {
            assert($field instanceof Field);

            if ($field->getNumber() === $number) {
                return $field;
            }
        }

        return null;
    }

    /**
     * @param Field[] $items
     */
    private static function assertItemsAreFields(array $items): void
    {
        foreach ($items as $item) {
            if (!$item instanceof Field) {
                throw new \InvalidArgumentException(sprintf('Row should has only items of %s.', Field::class));
            }
        }
    }
}

<?php

namespace App\Module\CSVSheetParser;

use App\Module\CSVSheetParser\Exception\FileIsNotExistException;
use App\Module\CSVSheetParser\Sheet\Row;

interface CsvFileParserInterface
{
    /**
     * @throws FileIsNotExistException
     *
     * @return Row[]|\Generator
     */
    public function parseFile(\SplFileInfo $sourceFileInfo): \Generator;
}

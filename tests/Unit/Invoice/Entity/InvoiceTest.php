<?php

namespace App\Tests\Unit\Invoice\Entity;

use App\Invoice\Entity\Invoice;
use Money\Currency;
use Money\Money;
use PHPUnit\Framework\TestCase;

final class InvoiceTest extends TestCase
{
    public function testInvoiceCanBeCreatedWithExpectedData(): void
    {
        $expectedId = 'expected id';
        $expectedAmount = new Money('10040', new Currency('USD'));
        $expectedUploadedOn = new \DateTimeImmutable();
        $expectedDueOn = $expectedUploadedOn->modify('-1 day');

        $invoice = new Invoice($expectedId, $expectedAmount, $expectedUploadedOn, $expectedDueOn);

        $this->assertSame($expectedId, $invoice->getId());
        $this->assertTrue($expectedAmount->equals($invoice->getAmount()));
        $this->assertSame($expectedUploadedOn, $invoice->getUploadedOn());
        $this->assertSame($expectedDueOn, $invoice->getDueOn());
        $this->assertNotEmpty($invoice->getSellPrice());
    }

    public function testInvoiceCannotBeCreatedWithNegativeAmount(): void
    {
        $this->expectException(\LogicException::class);
        $this->expectExceptionMessage('Amount must not be negative');

        $today = new \DateTimeImmutable();
        $negativeAmount = new Money('-100', new Currency('USD'));

        new Invoice('id', $negativeAmount, $today, $today);
    }

    public function testInvoiceSellPriceIsCalculatedCorrectlyForEarlyUploadedInvoice(): void
    {
        $today = new \DateTimeImmutable();
        $thirtyOneDaysAgo = $today->modify('-31 days');

        $amount = new Money('10000', new Currency('USD'));
        $expectedSellPriceAmount = new Money('5000', new Currency('USD'));

        $invoice = new Invoice('id', $amount, $thirtyOneDaysAgo, $today);

        $this->assertTrue($expectedSellPriceAmount->equals($invoice->getSellPrice()));
    }

    public function testInvoiceSellPriceIsCalculatedCorrectlyForLateUploadedInvoice(): void
    {
        $today = new \DateTimeImmutable();
        $thirtyDaysAgo = $today->modify('-30 days');

        $amount = new Money('10000', new Currency('USD'));
        $expectedSellPriceAmount = new Money('3000', new Currency('USD'));

        $invoice = new Invoice('id', $amount, $thirtyDaysAgo, $today);

        $this->assertTrue($expectedSellPriceAmount->equals($invoice->getSellPrice()));
    }
}

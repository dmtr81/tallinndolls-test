<?php

namespace App\Tests\Unit\Invoice\Import\Report;

use App\Invoice\Import\Report\InMemoryInvoicesImportLogger;
use PHPUnit\Framework\TestCase;

final class InMemoryInvoicesImportLoggerTest extends TestCase
{
    private InMemoryInvoicesImportLogger $inMemoryInvoicesImportLogger;

    protected function setUp(): void
    {
        parent::setUp();

        $this->inMemoryInvoicesImportLogger = new InMemoryInvoicesImportLogger();
    }

    protected function tearDown(): void
    {
        unset($this->inMemoryInvoicesImportLogger);

        parent::tearDown();
    }

    public function testLoggerCanCreateReportForKnownSheet(): void
    {
        $expectedSheetFile = $this->createMock(\SplFileInfo::class);

        $expectedFailureMessage = 'some message';
        $expectedNumberOfSuccessfulImportedInvoices = 1;

        $this->inMemoryInvoicesImportLogger->logFailedRowImport($expectedSheetFile, $expectedFailureMessage);
        $this->inMemoryInvoicesImportLogger->logSuccessfulRowImport($expectedSheetFile);

        $report = $this->inMemoryInvoicesImportLogger->getFreshReport($expectedSheetFile);

        $this->assertSame([$expectedFailureMessage], $report->getFailureMessages());
        $this->assertSame($expectedNumberOfSuccessfulImportedInvoices, $report->getNumberOfSuccessfulImportedInvoices());
    }

    public function testLoggerMustCreateEmptyReportForUnknownSheet(): void
    {
        $unknownSheetFile = $this->createMock(\SplFileInfo::class);

        $report = $this->inMemoryInvoicesImportLogger->getFreshReport($unknownSheetFile);

        $this->assertCount(0, $report->getFailureMessages());
        $this->assertEquals(0, $report->getNumberOfSuccessfulImportedInvoices());
    }
}

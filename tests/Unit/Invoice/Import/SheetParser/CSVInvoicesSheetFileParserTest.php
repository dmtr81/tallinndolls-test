<?php

namespace App\Tests\Unit\Invoice\Import\Report;

use App\Invoice\Import\SheetParser\CSVInvoicesSheetFileParser;
use App\Invoice\Import\SheetParser\InvoiceRow;
use App\Module\CSVSheetParser\CsvFileParserInterface;
use App\Module\CSVSheetParser\Sheet\Field;
use App\Module\CSVSheetParser\Sheet\Row;
use PHPUnit\Framework\TestCase;

final class CSVInvoicesSheetFileParserTest extends TestCase
{
    public function testParserMustGetFirstThreeColumnsToFormInvoiceRow(): void
    {
        $expectedId = 'id';
        $expectedAmount = '100.00';
        $expectedDueOn = '2020-02-02';

        $sheetRow = new Row([
            new Field(0, $expectedId),
            new Field(1, $expectedAmount),
            new Field(2, $expectedDueOn),
        ]);

        $csvFileParser = $this->createCsvFileParserToGetRow($sheetRow);

        $invoicesSheetFileParser = new CSVInvoicesSheetFileParser($csvFileParser);
        $invoicesRows = iterator_to_array($invoicesSheetFileParser->parseFile($this->createMock(\SplFileInfo::class)));

        $this->assertCount(1, $invoicesRows);

        $invoiceRow = current($invoicesRows);

        $this->assertInstanceOf(InvoiceRow::class, $invoiceRow);
        assert($invoiceRow instanceof InvoiceRow);

        $this->assertSame($expectedId, $invoiceRow->invoiceId);
        $this->assertSame($expectedAmount, $invoiceRow->invoiceAmount);
        $this->assertSame($expectedDueOn, $invoiceRow->invoiceDueOn);
    }

    public function testParserCanCreateEmptyInvoiceRowByEmptySheetRow(): void
    {
        $csvFileParser = $this->createCsvFileParserToGetRow(new Row([]));

        $invoicesSheetFileParser = new CSVInvoicesSheetFileParser($csvFileParser);
        $invoicesRows = iterator_to_array($invoicesSheetFileParser->parseFile($this->createMock(\SplFileInfo::class)));

        $this->assertCount(1, $invoicesRows);

        $invoiceRow = current($invoicesRows);

        $this->assertInstanceOf(InvoiceRow::class, $invoiceRow);
        assert($invoiceRow instanceof InvoiceRow);

        $this->assertEmpty($invoiceRow->invoiceId);
        $this->assertEmpty($invoiceRow->invoiceAmount);
        $this->assertEmpty($invoiceRow->invoiceDueOn);
    }

    private function createCsvFileParserToGetRow(Row $sheetRow): CsvFileParserInterface
    {
        $parserStub = $this->createMock(CsvFileParserInterface::class);
        $parserStub
            ->method('parseFile')
            ->willReturnCallback(fn () => yield $sheetRow)
        ;

        return $parserStub;
    }
}

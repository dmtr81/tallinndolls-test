<?php

namespace App\Tests\Acceptance;

use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

abstract class AcceptanceTestCase extends WebTestCase
{
    use FixturesTrait;

    private KernelBrowser $browser;

    protected function setUp(): void
    {
        parent::setUp();

        $this->browser = $this->createClient();
    }

    protected function tearDown(): void
    {
        unset($this->browser);

        parent::tearDown();
    }

    final function getBrowser(): KernelBrowser
    {
        return $this->browser;
    }
}

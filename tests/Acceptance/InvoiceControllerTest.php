<?php

namespace App\Tests\Acceptance;

use App\DataFixtures\DuedAndUploadedInvoiceOnSameDayFixture;
use App\Invoice\Entity\Invoice;
use Symfony\Component\HttpFoundation\Response;

final class InvoiceControllerTest extends AcceptanceTestCase
{
    private const INVOICES_LIST_PAGE_URL = '/';

    public function testCustomerCanSeeInvoicesPage(): void
    {
        $invoicesListPage = $this->getBrowser()->request('GET', self::INVOICES_LIST_PAGE_URL);

        $this->assertEquals(Response::HTTP_OK, $this->getBrowser()->getResponse()->getStatusCode());
        $this->assertNotEmpty($invoicesListPage->filter('h1:contains("Invoices")')->text());
    }

    public function testCustomerCanSeeUploadedInvoiceInListPage(): void
    {
        $referenceRepository = $this->loadFixtures([
            DuedAndUploadedInvoiceOnSameDayFixture::class,
        ])->getReferenceRepository();

        $invoice = $referenceRepository->getReference(DuedAndUploadedInvoiceOnSameDayFixture::REFERENCE_NAME);
        assert($invoice instanceof Invoice);

        $invoicesListPage = $this->getBrowser()->request('GET', self::INVOICES_LIST_PAGE_URL);

        $this->assertStringContainsString($invoice->getId(), $invoicesListPage->filter('table')->text());
    }

    public function testCustomerCanUploadInvoicesSheetAndSeeReport(): void
    {
        $invoicesListPage = $this->getBrowser()->request('GET', self::INVOICES_LIST_PAGE_URL);

        $uploadInvoicesForm = $invoicesListPage->selectButton('Import')->form();
        $uploadInvoicesForm->setValues([
            'upload_invoices_sheet_form[invoicesSheetFile]' => $this->getValidCSVInvoicesSheetFile(),
        ]);

        $reportPage = $this->getBrowser()->submit($uploadInvoicesForm);

        $this->assertEquals(Response::HTTP_OK, $this->getBrowser()->getResponse()->getStatusCode());
        $this->assertNotEmpty($reportPage->filter('h1:contains("Import report")')->text());

        $reportPageText = $reportPage->text();

        $this->assertStringContainsString('Added: ', $reportPageText);
        $this->assertStringContainsString('Errors: ', $reportPageText);
    }

    private function getValidCSVInvoicesSheetFile(): \SplFileInfo
    {
        return new \SplFileInfo($this->getContainer()->getParameter('invoices_csv_sheet_filename'));
    }
}

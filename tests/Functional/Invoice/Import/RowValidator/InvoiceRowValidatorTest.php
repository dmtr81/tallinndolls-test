<?php

namespace App\Tests\Functional\Invoice\Import\RowValidator;

use App\DataFixtures\DuedAndUploadedInvoiceOnSameDayFixture;
use App\Invoice\Entity\Invoice;
use App\Invoice\Import\RowValidator\Exception\InvoiceRowIsInvalidException;
use App\Invoice\Import\RowValidator\InvoiceRowValidatorInterface;
use App\Invoice\Import\SheetParser\InvoiceRow;
use App\Tests\Functional\FunctionalTestCase;

final class InvoiceRowValidatorTest extends FunctionalTestCase
{
    private InvoiceRowValidatorInterface $validator;

    protected function setUp(): void
    {
        parent::setUp();

        $this->loadFixtures([]); // to clear invoices

        $this->validator = $this->getContainer()->get(InvoiceRowValidatorInterface::class);
    }

    protected function tearDown(): void
    {
        unset($this->validator);

        parent::tearDown();
    }

    public function testValidatorMustThrowExceptionForInvalidRow(): void
    {
        $this->expectException(InvoiceRowIsInvalidException::class);

        $invalidRow = new InvoiceRow();

        $this->validator->assertRowValidity($invalidRow);
    }

    public function testValidatorMustNotThrowExceptionForValidRow(): void
    {
        $validRow = $this->createValidInvoiceRow();

        $actualException = null;

        try {
            $this->validator->assertRowValidity($validRow);
        } catch (\Throwable $exception) {
            $actualException = $exception;
        }

        $this->assertNull($actualException);
    }

    /**
     * @dataProvider getInvoiceRowsWithOneEmptyField
     */
    public function testAllRowPropertyMustNotBeBlank(InvoiceRow $rowWithEmptyField): void
    {
        $this->expectException(InvoiceRowIsInvalidException::class);
        $this->expectExceptionMessage('This value should not be blank');

        $this->validator->assertRowValidity($rowWithEmptyField);
    }

    public function getInvoiceRowsWithOneEmptyField(): \Generator
    {
        $validRow = $this->createValidInvoiceRow();

        $rowWithEmptyId = clone $validRow;
        $rowWithEmptyId->invoiceId = null;

        yield [$rowWithEmptyId];

        $rowWithEmptyAmount = clone $validRow;
        $rowWithEmptyAmount->invoiceAmount = null;

        yield [$rowWithEmptyAmount];

        $rowWithEmptyDueOn = clone $validRow;
        $rowWithEmptyDueOn->invoiceDueOn = null;

        yield [$rowWithEmptyDueOn];
    }

    public function testIdMustLessThen255(): void
    {
        $this->expectException(InvoiceRowIsInvalidException::class);
        $this->expectExceptionMessage('This value is too long');

        $invoiceRow = $this->createValidInvoiceRow();
        $invoiceRow->invoiceId = str_repeat('c', 256);

        $this->validator->assertRowValidity($invoiceRow);
    }

    public function testInvoiceIdMustBeNotExistedById(): void
    {
        $this->expectException(InvoiceRowIsInvalidException::class);
        $this->expectExceptionMessage('The invoice already exists');

        $referenceRepository = $this->loadFixtures([
            DuedAndUploadedInvoiceOnSameDayFixture::class,
        ])->getReferenceRepository();

        $existedInvoice = $referenceRepository->getReference(DuedAndUploadedInvoiceOnSameDayFixture::REFERENCE_NAME);
        assert($existedInvoice instanceof Invoice);

        $invoiceRow = $this->createValidInvoiceRow();
        $invoiceRow->invoiceId = $existedInvoice->getId();

        $this->validator->assertRowValidity($invoiceRow);
    }

    public function testAmountMustBeString(): void
    {
        $this->expectException(InvoiceRowIsInvalidException::class);
        $this->expectExceptionMessage('This value should be of type string.');

        $invoiceRow = $this->createValidInvoiceRow();
        $invoiceRow->invoiceAmount = 50.05;

        $this->validator->assertRowValidity($invoiceRow);
    }

    public function testAmountMustBeValidAmount(): void
    {
        $this->expectException(InvoiceRowIsInvalidException::class);
        $this->expectExceptionMessage('Value must be a valid amount.');

        $invoiceRow = $this->createValidInvoiceRow();
        $invoiceRow->invoiceAmount = 'invalid amount';

        $this->validator->assertRowValidity($invoiceRow);
    }

    public function testAmountMustBeNotBeNegative(): void
    {
        $this->expectException(InvoiceRowIsInvalidException::class);
        $this->expectExceptionMessage('This value should be either positive or zero.');

        $invoiceRow = $this->createValidInvoiceRow();
        $invoiceRow->invoiceAmount = '-100';

        $this->validator->assertRowValidity($invoiceRow);
    }

    public function testDueDateMustBeValidDate(): void
    {
        $this->expectException(InvoiceRowIsInvalidException::class);
        $this->expectExceptionMessage('This value is not a valid date.');

        $invoiceRow = $this->createValidInvoiceRow();
        $invoiceRow->invoiceDueOn = 'invalid date';

        $this->validator->assertRowValidity($invoiceRow);
    }

    private function createValidInvoiceRow(): InvoiceRow
    {
        $validRow = new InvoiceRow();
        $validRow->invoiceId = 'id';
        $validRow->invoiceAmount = '100.10';
        $validRow->invoiceDueOn = '2020-02-02';

        return $validRow;
    }
}

<?php

namespace App\Tests\Functional\Invoice\Import;

use App\Invoice\Entity\Invoice;
use App\Invoice\Import\InvoicesSheetImporter;
use App\Invoice\Import\Report\InvoicesImportLoggerInterface;
use App\Invoice\Import\SheetParser\InvoiceRow;
use App\Invoice\Import\SheetParser\InvoicesSheetFileParserMock;
use App\Invoice\Repository\InvoiceRepositoryInterface;
use App\Tests\Functional\FunctionalTestCase;
use Money\Currency;
use Money\MoneyParser;

final class InvoicesSheetImporterTest extends FunctionalTestCase
{
    private InvoicesSheetImporter $invoicesImporter;
    private InvoiceRepositoryInterface $invoiceRepository;
    private InvoicesImportLoggerInterface $invoiceImportLogger;
    private MoneyParser $moneyParser;

    protected function setUp(): void
    {
        parent::setUp();

        $this->loadFixtures([]); // to clear invoices

        $this->invoicesImporter = $this->getContainer()->get(InvoicesSheetImporter::class);
        $this->invoiceRepository = $this->getContainer()->get(InvoiceRepositoryInterface::class);
        $this->invoiceImportLogger = $this->getContainer()->get(InvoicesImportLoggerInterface::class);
        $this->moneyParser = $this->getContainer()->get(MoneyParser::class);
    }

    protected function tearDown(): void
    {
        unset(
            $this->invoicesImporter,
            $this->invoiceRepository,
            $this->invoiceImportLogger,
            $this->moneyParser,
        );

        parent::tearDown();
    }

    public function testValidRowsFromInvoiceParserMustBeSaveInRepository(): void
    {
        $validRow = $this->createValidInvoiceRow();
        $invoicesSheetFileParser = new InvoicesSheetFileParserMock($validRow);

        $this->invoicesImporter->importSheet($this->createMock(\SplFileInfo::class), $invoicesSheetFileParser);

        $actualInvoice = $this->invoiceRepository->findById($validRow->invoiceId);

        $this->assertInstanceOf(Invoice::class, $actualInvoice);
        assert($actualInvoice instanceof Invoice);

        $expectedAmount = $this->moneyParser->parse($validRow->invoiceAmount, new Currency('USD'));

        $this->assertSame($validRow->invoiceId, $actualInvoice->getId());
        $this->assertTrue($expectedAmount->equals($actualInvoice->getAmount()));
        $this->assertSame($validRow->invoiceDueOn, $actualInvoice->getDueOn()->format('Y-m-d'));
    }

    /**
     * @todo move to events subscriber test case
     */
    public function testSuccessfulImportedRowMustBeLogged(): void
    {
        $validRow = $this->createValidInvoiceRow();
        $invoicesSheetFileParser = new InvoicesSheetFileParserMock($validRow);

        $sheetFile = $this->createMock(\SplFileInfo::class);

        $this->invoicesImporter->importSheet($sheetFile, $invoicesSheetFileParser);

        $report = $this->invoiceImportLogger->getFreshReport($sheetFile);

        $this->assertEquals(1, $report->getNumberOfSuccessfulImportedInvoices());
        $this->assertCount(0, $report->getFailureMessages());
    }

    public function testInvalidRowsMustNotBeSavedInRepository(): void
    {
        $invalidRow = $this->createInvalidInvoiceRow();
        $invoicesSheetFileParser = new InvoicesSheetFileParserMock($invalidRow);

        $this->invoicesImporter->importSheet($this->createMock(\SplFileInfo::class), $invoicesSheetFileParser);

        $actualInvoice = $this->invoiceRepository->findById($invalidRow->invoiceId);

        $this->assertNull($actualInvoice);
    }

    /**
     * @todo move to events subscriber test case
     */
    public function testFailedImportOfRowMustBeAlsoLogged(): void
    {
        $invalidRow = $this->createInvalidInvoiceRow();
        $invoicesSheetFileParser = new InvoicesSheetFileParserMock($invalidRow);

        $sheetFile = $this->createMock(\SplFileInfo::class);

        $this->invoicesImporter->importSheet($sheetFile, $invoicesSheetFileParser);

        $report = $this->invoiceImportLogger->getFreshReport($sheetFile);

        $this->assertEquals(0, $report->getNumberOfSuccessfulImportedInvoices());
        $this->assertCount(1, $report->getFailureMessages());
    }

    private function createValidInvoiceRow(): InvoiceRow
    {
        $validRow = new InvoiceRow();
        $validRow->invoiceId = 'id';
        $validRow->invoiceAmount = '100.10';
        $validRow->invoiceDueOn = '2020-12-01';

        return $validRow;
    }

    private function createInvalidInvoiceRow(): InvoiceRow
    {
        $invalidRow = new InvoiceRow();
        $invalidRow->invoiceId = 'some id';

        return $invalidRow;
    }
}
